from django.db import models
from django.utils import timezone

class PerfilUsuario(models.Model):
    nombre = models.CharField(max_length=50, verbose_name="Nombre")
    usuario_Id = models.CharField(max_length=25, verbose_name="UsuarioId")
    clave = models.CharField(max_length=100, verbose_name="Clave")
    telefono = models.CharField(max_length=20, verbose_name="Teléfono" , null=True, default=None, blank=True)
    fecha_nacimiento = models.DateField(verbose_name="Fecha nacimiento" , null=True, default=None, blank=True)
    ultima_conexion = models.DateTimeField(verbose_name="Fecha de la última conexion" , null=True, default=None, blank=True)
    email = models.EmailField(verbose_name="Email")
    anos_antiguedad = models.IntegerField(verbose_name="Antiguedad", default=0)
    fecha_creacion = models.DateField(verbose_name="Fecha de creación", auto_now_add=True)
    def __str__ (self):
        return self.nombre

class Proyecto(models.Model):
    titulo = models.CharField(max_length=50, verbose_name="Titulo")
    descripcion = models.CharField(max_length=1000, verbose_name="Descripción")
    nombre_cliente = models.CharField(max_length=1000, verbose_name="Nombre Cliente")
    def __str__ (self):
        return self.titulo

class Supervisor(PerfilUsuario):
    especialisacion = models.CharField(max_length=50, verbose_name="Especialisación")        

class Desarrollador(PerfilUsuario):
    jefe = models.ForeignKey(Supervisor, verbose_name="Supervisor", on_delete=models.CASCADE,)

class Tarea(models.Model):
    titulo = models.CharField(max_length=50, verbose_name="Titulo")
    descripcion = models.CharField(max_length=1000, verbose_name="Descripción")
    tiempo_transcurrido = models.IntegerField(verbose_name="Tiempo Transcurrido" , null=True, default=None, blank=True)
    importancia = models.IntegerField(verbose_name="Importancia")
    proyecto = models.ForeignKey(Proyecto, verbose_name="Proyecto" , null=True, default=None, blank=True, on_delete=models.CASCADE,)
    desarrollador = models.ForeignKey(Desarrollador, verbose_name="Usuario", on_delete=models.CASCADE,)
    def __str__ (self):
        return self.titulo                                 