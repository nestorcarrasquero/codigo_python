from django.shortcuts import render
from TareasControl.models import Proyecto

def saludo3(request):
    saludo = "....¡¡¡¡ Hola Mundo !!!!"
    edad = 15
    arreglo_capitales = ["Paris","London","Washington"]
    return render(request, "TareasControl/saludo3.html",{"saludo":saludo,"edad":edad,"arreglo_capitales":arreglo_capitales})

def conexion(request):
    return render(request,'TareasControl/conexion.html')

def saludo4(request):
    return render(request,'TareasControl/saludo4.html')

def proyectos(request):
    todo_projectos = Proyecto.objects.all()
    return render(request, 'TareasControl/proyectos.html', 
          {'action': "Desplegar todos los proyectos", 
           'todo_proyectos': todo_projectos})

def proy_oscar(request):
    accion="Desplegar proyectos de Oscar"
    todo_proyectos = Proyecto.objects.filter(nombre_cliente="Oscar")
    return render(request, 'TareasControl/proyectos.html', locals())  

def un_proy(request, pk):
    proyecto = Proyecto.objects.get(id=pk)
    return render(request, 'TareasControl/proydetalle.html', locals())   
    
def pag1(request):
    nuevo_proyecto = Proyecto(titulo="Manejo de tareas con Django", descripcion="Proyecto en Django.", nombre_cliente="Yo")
    nuevo_proyecto.save()
    return render(request, 'TareasControl/info1.html', {'action':'Guardar datos del modelo'})

def pag2(request):
    todo_proyectos = Proyecto.objects.all()
    return render(request, 'TareasControl/info2.html', {'action':'Desplegar todos los proyectos', 'todo_proyectos':todo_proyectos})

def pag3(request):
    action='Desplegar proyectos de "Yo"'
    todo_proyectos = Proyecto.objects.filter(nombre_cliente='Yo')
    return render(request, 'TareasControl/info2.html', locals())

def pag4(request):
    action='Desplegar proyectos de "Yo" y con título “Proyecto2”'
    todo_proyectos = Proyecto.objects.filter(nombre_cliente='Yo', titulo='Proyecto2')
    return render(request, 'TareasControl/info2.html', locals())    

def pag5(request):
    action='Desplegar el proyecto con id igual a 1'
    proyecto = Proyecto.objects.get(id="1")
    return render(request, 'TareasControl/info3.html', locals())

def pag6(request,pk):
    try:
        proyecto = Proyecto.objects.get(id=pk)
        action='Desplegar el proyecto con id:'+str(pk)
        return render(request, 'TareasControl/info3.html', locals())
    except:
        action='No existe el proyecto con el id:'+str(pk)
        return render(request, 'TareasControl/info3.html', locals())

def pag7(request):
    # Guardando un supervisor
    nvo_supervisor = Supervisor(nombre="Carla Carora", usuario_Id="carla", clave="carla", ultima_conexion=timezone.now(), email="carla@a.com", especialisacion="Python")
    nvo_supervisor.save()
    # Guardando un desarrollador
    nvo_desarrollador = Desarrollador(nombre="Yo", usuario_Id="yo", clave="yo", ultima_conexion=timezone.now(), email="yo@a.com", jefe=nvo_supervisor)
    nvo_desarrollador.save()
    # Guardando una tarea
    proyecto_a_enlazar = Proyecto.objects.get(id = 1)
    nva_tarea = Tarea(titulo="Agregando relación", descripcion="Ejemplo     de agregar una relación y guardarla", tiempo_transcurrido=2, importancia=0,
    proyecto=proyecto_a_enlazar, desarrollador=nvo_desarrollador)
    nva_tarea.save()
    return render(request, 'TareasControl/info1.html', {'action' : 'Guardar relación'})

def pag8(request):
    nvo_proyecto = Proyecto(titulo = "Proyecto Actualización", descripcion="Actualizar modelo. Este proyecto con la Tarea id=1 ", nombre_cliente="Gente")
    nvo_proyecto.save()
    tarea = Tarea.objects.get(id = 1)
    tarea.descripcion = "Ahora enlazado a proyecto id="+ str(nvo_proyecto.id)
    tarea.proyecto = nvo_proyecto
    tarea.save()
    return render(request, 'TareasControl/info1.html', {'action' : 'Actualización del modelo'})

def pag9(request):
    proyectos = Proyecto.objects.filter(nombre_cliente = "Gente").update(nombre_cliente="Nadie")
    return render(request, 'TareasControl/info1.html', {'action' : 'Actualización multiple'})                                       

def pag10(request):
    try:
        tarea = Tarea.objects.get(id = 2)
        tarea.delete()
        return render(request, 'TareasControl/info1.html', {'action' : 'Eliminación de la tarea 2'})
    except:
        return render(request, 'TareasControl/info1.html', {'action' : 'No existe la tarea 2 para eliminar'})

def pag11(request,pk):
    try:
        proyecto = Proyecto.objects.get(id = pk)
        tareas=Tarea.objects.filter(proyecto = proyecto)
        return render(request, 'TareasControl/info4.html', {'action' : 'Tareas del proyecto '+proyecto.titulo, 'tareas':tareas})
    except:
        return render(request, 'TareasControl/info1.html', {'action' : 'No existe el proyecto con id '+str(pk)})

def pag12(request,pk):
    try:
        tarea = Tarea.objects.get(id = pk)
        proyecto=tarea.proyecto
        return render(request, 'TareasControl/info1.html', {'action' : 'El proyecto '+str(proyecto.id)+ ' de la tarea '+str(tarea.id)})
    except:
        return render(request, 'TareasControl/info1.html', {'action' : 'No existe la tarea '+str(pk)})                    

from django.db.models import Q
def pag13(request):
    action='Proyecto con el opeador OR. (Yo o Proyecto2)'
    todo_proyectos = Proyecto.objects.filter(Q(nombre_cliente='Yo') |  Q(titulo='Proyecto2'))
    return render(request, 'TareasControl/info2.html', locals())

def pag14(request):
    proyecto = Proyecto.objects.raw('SELECT * FROM tareascontrol_proyecto')[0]
    return render(request, 'TareasControl/info3.html', {'action':'Desplegar el primer proyecto usando SQL', 'proyecto':proyecto})            