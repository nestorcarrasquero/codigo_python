from django.contrib import admin
from .models import Supervisor, Desarrollador, Proyecto, Tarea

admin.site.register(Supervisor)
admin.site.register(Desarrollador)
admin.site.register(Proyecto)

class TareaAdmin(admin.ModelAdmin):
    fieldsets = [ (None, {'fields': ['descripcion']}), ('Valores', {'fields': ['tiempo_transcurrido','importancia']}),]
    
admin.site.register(Tarea, TareaAdmin)
