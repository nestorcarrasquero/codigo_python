from django.urls import path

from . import views

urlpatterns = [
    path ('', views.saludo3, name='saludo3'),
    path ('saludo3', views.saludo3, name='saludo3'),
    path ('conexion', views.conexion, name='conexion'),
    path ('saludo', views.saludo4, name='saludo4'),
    path ('proyectos', views.proyectos, name='proyectos'),
    path ('proy_oscar', views.proy_oscar, name='proy_oscar'),
    path ('un_proy_<int:pk>', views.un_proy, name='un_proy'), 
    path ('pag1', views.pag1, name='pag1'), 
    path ('pag2', views.pag2, name='pag2'),
    path ('pag3', views.pag3, name='pag3'),
    path ('pag4', views.pag4, name='pag4'), 
    path ('pag5', views.pag5, name='pag5'),
    path ('pag6_<int:pk>', views.pag6, name='pag6'), 
    path ('pag7', views.pag7, name='pag7'),
    path ('pag8', views.pag8, name='pag8'),
    path ('pag9', views.pag9, name='pag9'),
    path ('pag10', views.pag10, name='pag10'),
    path ('pag11', views.pag11, name='pag11'),
    path ('pag12', views.pag12, name='pag12'),
    path ('pag13', views.pag13, name='pag13'),
    path ('pag14', views.pag14, name='pag14'),
]